package auto_restart;

import java.util.Scanner;

/**
 *
 * @author Ermoshkin Oleg
 */





public class Auto_Restart {
    
    public static void main(String[] args) throws InterruptedException{
        
        String commands = null;
        String server = null;
        Scanner in = new Scanner(System.in);
        Server_Con test = new Server_Con("192.168.100.133",2346);
        Server_Restart server_restart = new Server_Restart();
        
        System.out.println("Enter Exit for Close Program");
        
        while (!"Exit".equals(commands)) {
            System.out.println(">   Enter your command:");
            
            commands = in.next();
            
            if ("connect".equals(commands)){
                System.out.println(">    Enter Name Server");
                server = in.next();
                switch (server){
                    case "test": test.connect();
                    break;
                    default: System.out.println(">  Server name Error");
                    break;
                }
            }
            
            if ("disconnect".equals(commands)){
                System.out.println(">    Enter Name Server");
                server = in.next();
                switch (server){
                    case "test": test.disconnect();
                    break;
                    default: System.out.println(">  Server name Error");
                    break;
                }
            }
            
            if ("statusservice".equals(commands)){
                System.out.println(">    Enter Name Server");
                server = in.next();
                switch (server){
                    case "test": test.statusservice();
                    break;
                    default: System.out.println(">  Server name Error");
                    break;
                }
            }            
           
            
            if ("stopservice".equals(commands)){
                System.out.println(">    Enter Name Server");
                server = in.next();
                switch (server){
                    case "test": test.stopservice();
                    break;
                    default: System.out.println(">  Server name Error");
                    break;
                }
            } 
            
            if ("startservice".equals(commands)){
                System.out.println(">    Enter Name Server");
                server = in.next();
                switch (server){
                    case "test": test.startservice();
                    break;
                    default: System.out.println(">  Server name Error");
                    break;
                }
            } 
            
            if ("restart".equals(commands)){
                System.out.println(">    Enter Name Server");
                server = in.next();
                switch (server){
                    case "test": 
                        if(!server_restart.Server_Restart("test")) {
                           System.out.print("Server restarted");
                        } else {
                           System.out.print("Server restart Error!");
                        }
                    break;
                    
                    case "itlxap1v": 
                        if(!server_restart.Server_Restart("itlxap1v")) {
                           System.out.print("Server restarted");
                        } else {
                           System.out.print("Server restart Error!");
                        }
                        break;
                        
                    case "itlxeqindexv": 
                        if(!server_restart.Server_Restart("itlxeqindexv")) {
                           System.out.print("Server restarted");
                        } else {
                           System.out.print("Server restart Error!");
                        }
                        break;
                        
                    default: 
                        System.out.println(">  Server name Error");
                        break;
                }
            }    
         
        }
        
        System.out.println("Close programm, Bye!");
        System.exit(0);
    }
 


}
