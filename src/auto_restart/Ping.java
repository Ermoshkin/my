package auto_restart;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author Ermoshkin Oleg
 */

public class Ping {
    public boolean status(String ip) throws UnknownHostException, IOException{
        int timeout = 2000;
        InetAddress addresses = InetAddress.getByName(ip);
        if ( addresses.isReachable(timeout))
            {
                return true; // just set a break point here
            }
        return false;
    }
}

