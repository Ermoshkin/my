package auto_restart;

import com.intellinx.repository.internal.GXEntityNode;
import com.intellinx.servicecontainer.GXIServiceRunner;
import com.intellinx.util.GXUID;
import java.net.InetSocketAddress;
import ru.iitdgroup.em.ITXServiceManager;
import ru.iitdgroup.em.ServiceContainer;

/**
 *
 * @author Ermoshkin Oleg
 */
public class Server_Con {
        public String ip;
        public int productVersion = 4;
        public String subVersion = "4.2";
        public String patchLevel = "SP3m";
        public String buildNumber="99999"; //Неважно
        public String buildDate = "2013-11-26 15:05:07.971";
        public String uid = "8016439E-B799-4B39-049E-BC6C122D982F";
        
        public InetSocketAddress inetSocketAddress;
        public ServiceContainer serviceContainer;
        public ITXServiceManager itxServiceManager;
        
        public Server_Con(String ip,int port){	
            this.ip = ip;
            inetSocketAddress = new InetSocketAddress(ip,port);
            serviceContainer= new ServiceContainer(inetSocketAddress, false);
            itxServiceManager =  new ITXServiceManager();
        }
       
        public void connect() {
          try {
              itxServiceManager.setServerVersion(productVersion,subVersion,patchLevel,buildNumber,buildDate);
              serviceContainer.connect("U_M0PBU","8j,tpmzyCbltkbYfDtnrt"); // тут юзер и пассворд
              System.out.println("Connected");
              } catch (Exception e){
                    e.printStackTrace();      
            }      
        }
        
        public void disconnect() {
            if (!serviceContainer.isConnected()){
                System.out.println("Server not connected");
                return;
            }
            serviceContainer.disconnect();
            System.out.println("Disconnected");
        }
        
        public void restartservice() throws InterruptedException {
            if (!serviceContainer.isConnected()){
                connect();
            }
            String Status = null;
            GXEntityNode gxEntityNode = (GXEntityNode)serviceContainer.getServicesRepository().getRepositoryExplorer().findNodeById(GXUID.fromString(uid));   //// получили компонент Intellinx (Rule Engine, DataChannel и тд и тп)
            GXIServiceRunner gxiServiceRunner=  itxServiceManager.getServiceRunner(gxEntityNode,serviceContainer);
            itxServiceManager.stopService(gxiServiceRunner);
            while (!"Stopped".equals(Status)){
                itxServiceManager.getState(gxiServiceRunner);
                Thread.sleep(1000L);
                System.out.print(".");
            }
            System.out.println(Status);
            
            itxServiceManager.startService(gxiServiceRunner);
            while (!"Running".equals(Status)){
                itxServiceManager.getState(gxiServiceRunner);
                Thread.sleep(1000L);
                System.out.print(".");
            }
            System.out.println(Status);
            System.out.println("Service Restarted");
            
            disconnect();
        }
        
        public void statusservice(){
          if (!serviceContainer.isConnected()){
                connect();
          }            
          GXEntityNode gxEntityNode = (GXEntityNode)serviceContainer.getServicesRepository().getRepositoryExplorer().findNodeById(GXUID.fromString(uid));   //// получили компонент Intellinx (Rule Engine, DataChannel и тд и тп)
          GXIServiceRunner gxiServiceRunner=  itxServiceManager.getServiceRunner(gxEntityNode,serviceContainer);
          System.out.println(itxServiceManager.getState(gxiServiceRunner));
          disconnect();
        }
        
        public void stopservice() throws InterruptedException{
            if (serviceContainer.isConnected() == false){
                connect();
            }
            System.out.println("Service UID: "+uid+" ");
            String Status = null;
            GXEntityNode gxEntityNode = (GXEntityNode)serviceContainer.getServicesRepository().getRepositoryExplorer().findNodeById(GXUID.fromString(uid));   //// получили компонент Intellinx (Rule Engine, DataChannel и тд и тп)
            GXIServiceRunner gxiServiceRunner=  itxServiceManager.getServiceRunner(gxEntityNode,serviceContainer);
            
            itxServiceManager.stopService(gxiServiceRunner);
            
            while (!"Stopped".equals(Status)){
                Status = itxServiceManager.getState(gxiServiceRunner).toString();
                Thread.sleep(1000);
                System.out.print(".");
            }
            
            System.out.println(Status);  
            disconnect();
        }
        
        public void startservice() throws InterruptedException{
            if (serviceContainer.isConnected() == false){
                connect();
            }
            System.out.println("Service UID: "+uid+" ");
            String Status = null;
            GXEntityNode gxEntityNode = (GXEntityNode)serviceContainer.getServicesRepository().getRepositoryExplorer().findNodeById(GXUID.fromString(uid));   //// получили компонент Intellinx (Rule Engine, DataChannel и тд и тп)
            GXIServiceRunner gxiServiceRunner=  itxServiceManager.getServiceRunner(gxEntityNode,serviceContainer);
            
            itxServiceManager.startService(gxiServiceRunner);
            
            while (!"Running".equals(Status)){
                Status = itxServiceManager.getState(gxiServiceRunner).toString();
                Thread.sleep(1000);
                System.out.print(".");
            }
            
            System.out.println(Status);  
            disconnect();
        }        
    
}
