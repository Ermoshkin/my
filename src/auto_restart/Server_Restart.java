package auto_restart;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author Ermoshkin Oleg
 */



public class Server_Restart {
    
    static Server_Con itlxap1v_DC_Alfa;
    static Server_Con itlxap1v_RE_EQ;
    static Server_Con itlxap1v_Backlog_Writer;
    
    static Server_Con test;
    
    static Server_Con itlxap3_Intellinx_EQ;
    static Server_Con itlxap4_Intellinx_EQ;
    
    static Server_Con itlxeqindexv_Indexer;
    
   /* 
    public static void main(){
        createservercon(); 
    }
    */
    public  Server_Restart(){   
        /*
        **    Контейнеры сервер itlxap1v
        */
        itlxap1v_DC_Alfa = new Server_Con("172.25.28.198",2346);
        itlxap1v_DC_Alfa.subVersion = "4.1";
        itlxap1v_DC_Alfa.patchLevel = "03";
        itlxap1v_DC_Alfa.uid = "6B867B82-70D1-48BB-2F51-B67A589F78A2";
        
        itlxap1v_RE_EQ = new Server_Con("172.25.28.198",2347);
        itlxap1v_RE_EQ.subVersion = "4.1";
        itlxap1v_RE_EQ.patchLevel = "03";
        itlxap1v_RE_EQ.uid = "98F7E5BD-BCF1-97E7-5BE3-4EF68FC24F42";
                   
        itlxap1v_Backlog_Writer = new Server_Con("172.25.28.198",2340);
        itlxap1v_Backlog_Writer.subVersion = "4.1";
        itlxap1v_Backlog_Writer.patchLevel = "03";
        itlxap1v_Backlog_Writer.uid = "969917CE-C0FF-B24E-B4D3-1E91ED7F4F2A";
        
        /*
        **    Контейнеры сервер itlxeqindexv
        */
        itlxeqindexv_Indexer = new Server_Con("172.25.28.184",2345);
        itlxeqindexv_Indexer.subVersion = "4.1";
        itlxeqindexv_Indexer.patchLevel = "02";
        itlxeqindexv_Indexer.uid = "ADD8605D-DF42-E348-0A60-9A90403FCC9E";
        
        
        /*
        **    Контейнеры сервер itlxap3
        */
        itlxap3_Intellinx_EQ = new Server_Con("172.25.180.184",2444);
        itlxap3_Intellinx_EQ.subVersion = "4.2";
        itlxap3_Intellinx_EQ.patchLevel = "SP3m";         
        
        itlxap4_Intellinx_EQ = new Server_Con("10.1.1.1",2444);
        itlxap4_Intellinx_EQ.subVersion = "4.2";
        itlxap4_Intellinx_EQ.patchLevel = "SP3m";    
        
        
        
        /*
        **      TEST
        */
        test = new Server_Con("192.168.100.133",2346);
    }

    public boolean Server_Restart(String nameserver) {
        switch (nameserver){
            case ("itlxap1v"): return itlxap1v();  
            case ("itlxeqindexv"): return itlxeqindexv();    
            case ("test"): return test();    
            default: System.out.println(">  Error Name Server");
                     break;
        }
        return false;
    }

    boolean itlxap1v() {
        Win_CMD cmd = new Win_CMD();
        Ping ping = new Ping();
        try {                
            itlxap3_Intellinx_EQ.stopservice();
            itlxap4_Intellinx_EQ.stopservice();
            itlxap1v_DC_Alfa.stopservice();
            itlxap1v_RE_EQ.stopservice();
            itlxap1v_Backlog_Writer.stopservice();
            cmd.exec("C:\\psservice.exe \\\\172.25.28.184 -u U_M0PBU -p 8j,tpmzyCbltkbYfDtnrt stop itxappserversvc");
            cmd.exec("shutdown /m \\\\172.25.28.198 /r /t 0");
            
            while(ping.status(itlxap1v_DC_Alfa.ip)){
               Thread.sleep(1_000); 
               System.out.print(".");
            }
            System.out.print("\nServer is down."); //Сервер был выключен
            
            while(ping.status(itlxap1v_DC_Alfa.ip)){
               Thread.sleep(1_000); 
               System.out.print(".");
            }
            System.out.println("Server is Running");
            cmd.exec("C:\\psservice.exe \\\\172.25.28.184 -u U_M0PBU -p 8j,tpmzyCbltkbYfDtnrt start itxappserversvc");
            //проверить http://itlxap1v:7780/IC_EQ Status:200
            itlxap1v_Backlog_Writer.startservice();
            itlxap1v_RE_EQ.startservice();
            itlxap1v_DC_Alfa.startservice();
            
            //Запустить сервисы на itlxap3 & 4
            
            return true;
            
            } catch (Exception e){
                e.printStackTrace();  
                return false;
        }
    }
        
    boolean itlxeqindexv() {
        Win_CMD cmd = new Win_CMD();
        Ping ping = new Ping();
        try {                
            itlxeqindexv_Indexer.stopservice();
            cmd.exec("C:\\psservice.exe \\\\172.25.28.184 -u U_M0PBU -p 8j,tpmzyCbltkbYfDtnrt stop intxEQIndexer");
            cmd.exec("shutdown /m \\\\172.25.28.184 /r /t 0");
            
            while(ping.status(itlxeqindexv_Indexer.ip)){
               Thread.sleep(1_000); 
               System.out.print(".");
            }
            System.out.print("\nServer is down."); //Сервер был выключен
            
            while(ping.status(itlxeqindexv_Indexer.ip)){
               Thread.sleep(1_000); 
               System.out.print(".");
            }
            System.out.println("Server is Running");
            
            itlxeqindexv_Indexer.statusservice();
            
            return true;
            
            } catch (Exception e){
                e.printStackTrace();  
                return false;
        }
    }
    
    
 boolean test() {
        Win_CMD cmd = new Win_CMD();
        Ping ping = new Ping();
        try {                
            test.stopservice();
            cmd.exec("C:\\psservice.exe \\\\192.168.100.133 -u oleg -p \"123\" stop intxTestACOL");
            cmd.exec("shutdown /m \\\\192.168.100.133 /r /t 0");
            
            while(ping.status(test.ip)){
               Thread.sleep(1_000); 
               System.out.print(".");
            }
            System.out.print("\nServer is down."); //Сервер был выключен
            
            while(!ping.status(test.ip)){
               Thread.sleep(1_000); 
               System.out.print(".");
            }
            
            
            System.out.println("Server is Running");
            
            cmd.exec("C:\\psservice.exe \\\\192.168.100.133 -u oleg -p \"123\" start intxTestACOL");
            
            System.out.print("Connecting to Intlx service port.");
            
            boolean connected = false;   //Долбимся к порту пока он не станет доступен.
            Socket socket = null;
            while (true) {
                try {
                        socket = new Socket("192.168.100.133",2346);
                        connected = true;
                    } catch (UnknownHostException ex) {
                        connected = false;
                    } catch (IOException ex) {
                        connected = false;
                    }
                if (connected) {
                        socket.close();
                        System.out.println("Connected");
                    } else {
                        try {
                            System.out.print(".");
                            Thread.sleep(500); // 0.5 sec
                        } catch (InterruptedException ex) {
                            break;
                        }
                    }
            }
            
            
            test.statusservice();
            
            return true;
            
            } catch (Exception e){
                e.printStackTrace();  
                return false;
        }
    }
      
}
