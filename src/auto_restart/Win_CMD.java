package auto_restart;

/**
 *
 * @author Ermoshkin Oleg
 */

import java.io.IOException;

public class Win_CMD {
    
    public static void exec(String command) throws IOException,
            InterruptedException {
        
        Process runtimeProcess;
        String executeCmd = "cmd /c "+command;
        int processComplete;
        
        while (true){          
        runtimeProcess = Runtime.getRuntime().exec(executeCmd);
        processComplete = runtimeProcess.waitFor();

        if (processComplete == 0) {
                System.out.println(executeCmd+" : Command Success!");
                break;
            }else{
                System.out.println(executeCmd+" : Command Error!");
                Thread.sleep(1_000);
                }
        }

    }
    
}