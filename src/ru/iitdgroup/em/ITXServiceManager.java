package ru.iitdgroup.em;

import com.intellinx.GXProductVersion;
import com.intellinx.repository.GXIEntityNode;
import com.intellinx.repository.internal.GXEntityNode;
import com.intellinx.servicecontainer.GXIServiceRunner;
import com.intellinx.servicecontainer.GXServiceState;
import com.intellinx.util.GXObjectUtil;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Date;
import java.util.Properties;

/**
 * Created by qzinWtf on 01.10.2016.
 */
public class ITXServiceManager {

    public void startService(GXIServiceRunner gxiServiceRunner){

 gxiServiceRunner.start(new GXIServiceRunner.GXIServiceStartCallbackHandler()
        {
            public void serviceStarted(GXIEntityNode serviceNode) {}

            public void serviceStartFailed(GXIEntityNode serviceNode, String message) {

                System.out.println("LOL! cannot start your service, bro");

            }
        });

    }

    public void stopService(GXIServiceRunner gxiServiceRunner){


        gxiServiceRunner.stop();

    }

    public void restartService(GXIServiceRunner gxiServiceRunner){

        ////тут надо подумать над ожиданием запуска
    }


    public  GXIServiceRunner getServiceRunner(GXEntityNode gxEntityNode, ServiceContainer serviceContainer){

        return serviceContainer.getServiceRunner(gxEntityNode);

    }

    public GXServiceState getState(GXIServiceRunner gxiServiceRunner){

        return gxiServiceRunner.getState();
    }


    public void setServerVersion(int productVersion, String subVersion,String patchLevel,String buildNumber,String buildDateStr) throws NoSuchMethodException, ParseException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {

        Class gxProductVersionClass = GXProductVersion.class;

        Constructor constructor = gxProductVersionClass.getDeclaredConstructor(int.class, String.class, String.class, String.class, Date.class, Properties.class);

        constructor.setAccessible(true);

     Date  buildDate = GXObjectUtil.parseDate(buildDateStr);

        GXProductVersion gxProductVersion = (GXProductVersion) constructor.newInstance(productVersion,subVersion,patchLevel,buildNumber,buildDate,null);


        Field mInstance=  gxProductVersionClass.getDeclaredField("m_instance");

        mInstance.setAccessible(true);

        mInstance.set(null,gxProductVersion);

        System.out.println(GXProductVersion.getProductVersion());




    }
}
