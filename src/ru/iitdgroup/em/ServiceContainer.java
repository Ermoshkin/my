package ru.iitdgroup.em;

import com.intellinx.jaas.authorization.spi.GXILogEventsReader;
import com.intellinx.perfmon.GXILogEvent;
import com.intellinx.repository.GXIEntityNode;
import com.intellinx.servicecontainer.GXIServiceListener;
import com.intellinx.GXProductFlags;
/*     */ import com.intellinx.GXProductVersion;
/*     */ import com.intellinx.compilation.GXIIdentifier;
/*     */ import com.intellinx.crypto.GXKeystoreEntry;
/*     */ import com.intellinx.deployment.GXIDeployedEntityNode;
/*     */ import com.intellinx.deployment.GXIDeploymentContext;
/*     */ import com.intellinx.deployment.GXIDeploymentContextListener;
/*     */ import com.intellinx.deployment.GXIRepositoryInformation;
/*     */ import com.intellinx.enterprisemanager.GXEnterpriseManagerPlugin;

/*     */ import com.intellinx.enterprisemanager.views.repository.GXEntityEditorInput;
/*     */ import com.intellinx.flow.GXCustomCounters;
/*     */ import com.intellinx.jaas.authentication.GXAuthenticationCancelled;
/*     */ import com.intellinx.jaas.authorization.GXIACLManager;

/*     */ import com.intellinx.jaas.authorization.spi.GXIResource;

/*     */ import com.intellinx.perfmon.GXIPerformanceTraceManager;
/*     */ import com.intellinx.remote.GXIRemoteSession;
/*     */ import com.intellinx.remote.GXIRemoteSession.GXISessionListener;
/*     */ import com.intellinx.remote.GXRemoteException;
/*     */ import com.intellinx.remote.GXRemoteSessionCache;
/*     */ import com.intellinx.repository.GXIEntityNode;
/*     */ import com.intellinx.repository.GXIRepository;
/*     */ import com.intellinx.repository.GXIRepositoryEntity;
/*     */ import com.intellinx.repository.GXIRepositoryEntityDescriptor;
/*     */ //import com.intellinx.servicecontainer.GXGetServiceContainerInfoTask.GeXGetLicenseInfoTask;
/*     */ import com.intellinx.servicecontainer.GXIServiceApi;
/*     */ import com.intellinx.servicecontainer.GXIServiceContainer;
/*     */ import com.intellinx.servicecontainer.GXIServiceContainer.GXIServiceContainerTask;
/*     */ import com.intellinx.servicecontainer.GXIServiceContainerConfiguration;
/*     */ import com.intellinx.servicecontainer.GXIServiceContainerConfigurationMetaData;
/*     */ import com.intellinx.servicecontainer.GXIServiceListener;
/*     */ import com.intellinx.servicecontainer.GXIServiceRunner;
/*     */ import com.intellinx.swt.GXMessageUtil;
/*     */ import com.intellinx.util.GXDateFormatUtil;
/*     */ import com.intellinx.util.GXExceptionUtil;
/*     */ import com.intellinx.util.GXIPreferences;
/*     */ import com.intellinx.util.GXNetworkUtil;
/*     */ import com.intellinx.util.GXPair;
/*     */ import com.intellinx.util.GXUID;
/*     */ import java.io.IOException;
/*     */ import java.net.InetAddress;
/*     */ import java.net.InetSocketAddress;
/*     */ import java.text.DateFormat;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collection;
/*     */ import java.util.Collections;
/*     */ import java.util.Date;
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import java.util.Properties;
/*     */ import javax.net.SocketFactory;
/*     */ import javax.security.auth.login.LoginException;


import java.net.InetSocketAddress;

/**
 * Created by qzinWtf on 30.09.2016.
 */
public class ServiceContainer implements GXIServiceListener {


        private static final String SERVICE_CONTAINER_REMOTE_OBJECT_NAME = "SERVICE_CONTAINER";
/*     */   private InetSocketAddress m_socketAddress;
/*     */   private GXIRemoteSession m_session;
/*     */   private GXIServiceContainer m_serviceContainer;
/*     */   private String m_name;
/*     */   private String m_description;
/*     */   private String m_displayName;

/*  77 */   private List<GXIServiceListener> m_serviceListeners = new ArrayList();

/*     */   private GXIResource m_containerResource;
/*     */   private boolean m_secured;
/*     */   private GXProductFlags m_productFlags;
/*     */

    /*     */   public ServiceContainer(InetSocketAddress socketAddress, boolean secured) {
/*  84 */     this.m_socketAddress = socketAddress;
/*  85 */     this.m_secured = secured;
/*  86 */
/*     */   }
    public GXIRepository getServicesRepository(){

        return m_serviceContainer.getServicesRepository();

    }

    public GXIRepository getRepository(){

        return m_serviceContainer.getRepository();

    }



    /*     */
/*     */   private String buildTitle() {
/*  93 */     if (!isConnected()) {
/*  94 */       return String.format("%s:%d", new Object[] { getHostName(), Integer.valueOf(this.m_socketAddress.getPort()) });
/*     */     }
/*  96 */     return String.format("%s on %s:%d", new Object[] { getName(), getHostName(), Integer.valueOf(this.m_socketAddress.getPort()) });
/*     */   }
/*     */
/*     */   private String getHostName()
/*     */   {
/* 101 */     return this.m_socketAddress.getAddress() != null ? this.m_socketAddress.getAddress().getHostName() : this.m_socketAddress.getHostName();
/*     */   }
/*     */
/*     */   public InetSocketAddress getSocketAddress() {
/* 105 */     return this.m_socketAddress;
/*     */   }
/*     */
/*     */   public void setSocketAddress(InetSocketAddress socketAddress) {
/* 109 */     this.m_socketAddress = socketAddress;
/*     */   }

/*     */
/*     */   public Collection<GXIIdentifier> getFlowIdentifiers()
/*     */   {
/* 118 */     return getRemoteServiceContainer().getFlowIdentifiers();
/*     */   }
/*     */
/* 121 */   private boolean m_innerSessionClosed = false;
/*     */   private GXIRemoteSession.GXISessionListener m_remoteSessionListener;
/*     */
/* 124 */   private void closeRemoteSession() { this.m_innerSessionClosed = true;
/* 125 */     if ((this.m_session != null) && (this.m_session.isOpen())) {
/* 126 */       this.m_session.disconnect();
/*     */     }
/* 128 */     this.m_session = null;
/*     */   }
/*     */
/*     */   private class RemoteSessionListener implements GXIRemoteSession.GXISessionListener
/*     */   {
            /*     */     private String m_name;
            /*     */     private String m_hostName;
            /*     */     private int m_hostPort;
            /*     */
/*     */     public RemoteSessionListener(String m_name, String m_hostName, int m_hostPort) {
/* 138 */       this.m_name = m_name;
/* 139 */       this.m_hostName = m_hostName;
/* 140 */       this.m_hostPort = m_hostPort;
/*     */     }

    @Override
    public void sessionDisconnected(GXIRemoteSession gxiRemoteSession) {

    }
            /*     */

/*     */   }
/*     */
/*     */
/*     */   public void connect(String user,String password)
/*     */     throws Exception
/*     */   {
/* 165 */     if (isConnected()) {
/* 166 */       return;
/*     */     }
/*     */     try {
/* 169 */       if (this.m_socketAddress.getAddress() == null) {
/* 170 */         throw new Exception("Remote server is unavailable");
/*     */       }
/* 172 */       this.m_innerSessionClosed = false;
        GXRemoteSessionCache.GXIRemoteSessionCallbackHandlerFactory gxiRemoteSessionCallbackHandlerFactory = new GXRemoteSessionCache.GXDefaultRemoteSessionCallbackHandlerFactory(user,password);

/* 173 */       GXRemoteSessionCache sessionCache =  new GXRemoteSessionCache(gxiRemoteSessionCallbackHandlerFactory);
        /* 174 */       SocketFactory socketFactory = GXNetworkUtil.getSocketFactory(this.m_secured);
/* 175 */       this.m_session = sessionCache.attachRemoteSession(socketFactory, this.m_socketAddress.getAddress(), this.m_socketAddress.getPort());
/*     */
/* 177 */       this.m_serviceContainer = ((GXIServiceContainer)this.m_session.getAnchor("SERVICE_CONTAINER"));
/* 178 */       this.m_serviceContainer.addServiceListener(this);


/* 185 */       this.m_remoteSessionListener = new RemoteSessionListener(getName(), getHostName(), this.m_socketAddress.getPort());
/* 186 */       this.m_session.addListener(this.m_remoteSessionListener);
/*     */     } catch (GXAuthenticationCancelled ce) {
/* 188 */       closeRemoteSession();
/*     */     } catch (IOException e) {
/* 190 */       closeRemoteSession();

/* 192 */       throw e;
/*     */     } catch (LoginException e) {
/* 194 */       closeRemoteSession();

/* 196 */       throw e;
/*     */     } catch (GXRemoteException e) {
/* 198 */       closeRemoteSession();

/* 200 */       throw e;
/*     */     } catch (Exception e) {
/* 202 */       closeRemoteSession();

/* 204 */       throw e;
/*     */     }
/*     */   }
/*     */
/*     */   public boolean isSystemConfKeyExists() {
/* 209 */     return getRemoteServiceContainer().isSystemConfKeyExists();
/*     */   }
/*     */
/*     */   public boolean changeSysConfKey(String password) {
/* 213 */     return getRemoteServiceContainer().changeSysConfKey(password);
/*     */   }
/*     */
/*     */   public boolean isPasswordEqualsToSysConfPassowrd(String password) {
/* 217 */     return getRemoteServiceContainer().isPasswordEqualsToSysConfPassowrd(password);
/*     */   }
/*     */

/*     */
/*     */   public void disconnect() {
/* 245 */     if (!isConnected()) {
/* 246 */       return;
/*     */     }
/*     */     try {
/* 249 */       this.m_serviceContainer.removeServiceListener(this);
/*     */     }
/*     */     catch (Throwable localThrowable) {}

/*     */
/* 258 */     if (this.m_session != null) {
/* 259 */       if (this.m_remoteSessionListener != null) {
/* 260 */         this.m_session.removeListener(this.m_remoteSessionListener);
/*     */       }
/* 262 */       //GXRemoteSessionCache sessionCache = GXEnterpriseManagerPlugin.getDefault().getSessionCache();
/* 263 */       // sessionCache.detachRemoteSession(this.m_session);
/*     */     }
/* 265 */     this.m_session = null;
/* 266 */     this.m_serviceContainer = null;
/* 267 */     this.m_name = null;
/* 268 */     this.m_description = null;
/* 269 */     this.m_productFlags = null;

/*     */   }
/*     */
/*     */   public boolean isConnected() {
/* 275 */     return (this.m_session != null) && (this.m_serviceContainer != null);
/*     */   }
/*     */





/*     */
/*     */   public int hashCode()
/*     */   {
/* 312 */     return this.m_socketAddress.hashCode();
/*     */   }
/*     */
/*     */   private GXIServiceContainer getRemoteServiceContainer() throws GXRemoteException {
/* 316 */     if (!isConnected()) {
/* 317 */       throw new GXRemoteException("Not connected to server");
/*     */     }
/* 319 */     return this.m_serviceContainer;
/*     */   }
/*     */
/*     */   public String getName() {
/* 323 */     if (this.m_name == null) {
/* 324 */       this.m_name = getRemoteServiceContainer().getName();
/*     */     }
/* 326 */     return this.m_name;
/*     */   }
/*     */
/*     */   public String getDescription() {
/* 330 */     if (this.m_description == null) {
/* 331 */       this.m_description = getRemoteServiceContainer().getName();
/*     */     }
/* 333 */     return this.m_description;
/*     */   }
/*     */
/*     */   public String getDisplayName() {
/* 337 */     if (this.m_displayName == null) {
/* 338 */       this.m_displayName = getRemoteServiceContainer().getName();
/*     */     }
/* 340 */     return this.m_displayName;
/*     */   }
/*     */
/*     */   public GXProductVersion getProductVersion() {
/* 344 */     return getRemoteServiceContainer().getProductVersion();
/*     */   }
/*     */
/*     */   public GXIServiceContainerConfigurationMetaData getConfigurationMetaData() {
/* 348 */     return getRemoteServiceContainer().getConfigurationMetaData();
/*     */   }
/*     */
/*     */   public GXIServiceContainerConfiguration getConfig() {
/* 352 */     return getRemoteServiceContainer().getConfig();
/*     */   }
/*     */
/*     */   public void setConfig(GXIServiceContainerConfiguration config) {
/* 356 */     getRemoteServiceContainer().setConfig(config);
/*     */   }
/*     */
/*     */   public GXIServiceApi getGlobalServiceApi(String serviceName) {
/* 360 */     return getRemoteServiceContainer().getGlobalServiceApi(serviceName);
/*     */   }

/*     */
/*     */   private class GXUIDeploymentContext implements GXIDeploymentContext, GXIDeploymentContextListener { private GXIDeploymentContext m_remoteDeploymentContext;
            /*     */
/*     */     private GXUIDeploymentContext() {}
            /*     */
/* 373 */     private Map<GXUID, GXIDeployedEntityNode> m_deployedEntitiesCache = Collections.synchronizedMap(new HashMap());
            /* 374 */     private List<GXIDeploymentContextListener> m_deploymentContextListeners = new ArrayList();
            /*     */
/*     */     private GXIDeploymentContext getRemoteDeploymentContext() {
/* 377 */       if (this.m_remoteDeploymentContext == null) {
/* 378 */
/* 379 */         this.m_remoteDeploymentContext.addDeploymentContextListener(this);
/*     */       }
/* 381 */       return this.m_remoteDeploymentContext;
/*     */     }
            /*     */
/*     */     private void clear() {
/* 385 */       if (this.m_remoteDeploymentContext != null) {
/*     */         try {
/* 387 */           this.m_remoteDeploymentContext.removeDeploymentContextListener(this);
/*     */         }
/*     */         catch (Throwable localThrowable) {}
/*     */       }
/* 391 */       this.m_deployedEntitiesCache.clear();
/* 392 */       this.m_remoteDeploymentContext = null;
/*     */     }
            /*     */
/*     */     public GXIDeployedEntityNode getDeployedEntity(GXUID guid) {
/* 396 */       if (this.m_deployedEntitiesCache.containsKey(guid)) {
/* 397 */         return (GXIDeployedEntityNode)this.m_deployedEntitiesCache.get(guid);
/*     */       }
/* 399 */       GXIDeployedEntityNode node = getRemoteDeploymentContext().getDeployedEntity(guid);
/* 400 */       this.m_deployedEntitiesCache.put(guid, node);
/* 401 */       return node;
/*     */     }
            /*     */
/*     */     public Collection<GXIDeployedEntityNode> getDeployedEntities(Collection<GXIRepositoryEntityDescriptor> types) {
/* 405 */       return getRemoteDeploymentContext().getDeployedEntities(types);
/*     */     }
            /*     */
/*     */     public com.intellinx.deployment.GXIDeployedEntity retrieveEntity(GXIDeployedEntityNode node) {
/* 409 */       return getRemoteDeploymentContext().retrieveEntity(node);
/*     */     }
            /*     */
/*     */     public GXIDeployedEntityNode deployEntity(GXIRepositoryEntity entity, GXIRepositoryInformation repositoryInformation) {
/* 413 */       GXIDeployedEntityNode node = getRemoteDeploymentContext().deployEntity(entity, repositoryInformation);
/* 414 */       this.m_deployedEntitiesCache.put(node.getEntityId(), node);
/* 415 */       return node;
/*     */     }
            /*     */
/*     */     public void undeployEntity(GXIDeployedEntityNode node) {
/* 419 */       getRemoteDeploymentContext().undeployEntity(node);
/* 420 */       this.m_deployedEntitiesCache.remove(node.getEntityId());
/*     */     }
            /*     */
/*     */     public void addDeploymentContextListener(GXIDeploymentContextListener listener) {
/* 424 */       if (!this.m_deploymentContextListeners.contains(listener)) {
/* 425 */         this.m_deploymentContextListeners.add(listener);
/*     */       }
/*     */     }
            /*     */
/*     */     public void removeDeploymentContextListener(GXIDeploymentContextListener listener) {
/* 430 */       this.m_deploymentContextListeners.remove(listener);
/*     */     }
            /*     */
/*     */     public void objectDeployed(GXIDeployedEntityNode node) {
/* 434 */       for (GXIDeploymentContextListener l : this.m_deploymentContextListeners) {
/* 435 */         l.objectDeployed(node);
/*     */       }
/*     */     }
            /*     */
/*     */     public void objectUnDeployed(GXIDeployedEntityNode node) {
/* 440 */       this.m_deployedEntitiesCache.remove(node.getEntityId());
/* 441 */       for (GXIDeploymentContextListener l : this.m_deploymentContextListeners) {
/* 442 */         l.objectUnDeployed(node);
/*     */       }
/*     */     }
/*     */   }

/*     */
/*     */   public void addServiceListener(GXIServiceListener listener) {
/* 456 */     if (!this.m_serviceListeners.contains(listener)) {
/* 457 */       this.m_serviceListeners.add(listener);
/*     */     }
/*     */   }
/*     */
/*     */   public void removeServiceListener(GXIServiceListener listener) {
/* 462 */     this.m_serviceListeners.remove(listener);
/*     */   }
/*     */
/*     */   public GXIServiceRunner getServiceRunner(GXIEntityNode node) {
/* 466 */     if ((getRemoteServiceContainer() == null) || (node == null)) {
/* 467 */       return null;
/*     */     }
/* 469 */     return getRemoteServiceContainer().getServiceRunner(node);
/*     */   }
/*     */
/*     */   public Object runTask(GXIServiceContainer.GXIServiceContainerTask task) throws Exception {
/* 473 */     return getRemoteServiceContainer().runTask(task);
/*     */   }
/*     */

/*     */

/*     */
/*     */   public Properties getSystemProperties() {
/* 530 */     return getRemoteServiceContainer().getSystemProperties();
/*     */   }
/*     */
/*     */   public boolean isClassAvailable(String className) {
/* 534 */     return getRemoteServiceContainer().isClassAvailable(className);
/*     */   }
/*     */
/*     */   public boolean isGlobalServiceAvailable(String serviceName) {
/* 538 */     return getRemoteServiceContainer().isGlobalServiceAvailable(serviceName);
/*     */   }
/*     */
/*     */   public GXIResource getContainerResource() {
/* 542 */     if (this.m_containerResource == null) {
/*     */       try {
/* 544 */         this.m_containerResource = getRemoteServiceContainer().getContainerResource();
/*     */       } catch (GXRemoteException e) {
/* 546 */         this.m_serviceContainer = null;
/* 547 */         throw e;
/*     */       }
/*     */     }
/* 550 */     return this.m_containerResource;
/*     */   }
/*     */
/*     */   public GXIACLManager getACLManager() {
/*     */     try {
/* 555 */       return getRemoteServiceContainer().getACLManager();
/*     */     } catch (GXRemoteException e) {
/* 557 */       this.m_serviceContainer = null;
/* 558 */       throw e;
/*     */     }
/*     */   }
/*     */
/*     */   public GXILogEventsReader<GXILogEvent.GXIAccessLogEvent> getAccessLogReader() {
/*     */     try {
/* 564 */       return getRemoteServiceContainer().getAccessLogReader();
/*     */     } catch (GXRemoteException e) {
/* 566 */       this.m_serviceContainer = null;
/* 567 */       throw e;
/*     */     }
/*     */   }
/*     */
/*     */   public void serviceStateChanged(GXIEntityNode node) {
/* 572 */     for (GXIServiceListener l : this.m_serviceListeners) {
/* 573 */       l.serviceStateChanged(node);
/*     */     }
/*     */   }
/*     */
/*     */   public GXProductFlags getProductFlags() {
/*     */     try {
/* 579 */       if (this.m_productFlags == null) {
/* 580 */         this.m_productFlags = getRemoteServiceContainer().getProductFlags();
/*     */       }
/* 582 */       return this.m_productFlags;
/*     */     } catch (GXRemoteException e) {
/* 584 */       this.m_serviceContainer = null;
/* 585 */       throw e;
/*     */     }
/*     */   }
/*     */
/*     */   public GXKeystoreEntry getPDFSignKeystoreEntry()
/*     */   {
/*     */     try {
/* 592 */       return getRemoteServiceContainer().getPDFSignKeystoreEntry();
/*     */     } catch (GXRemoteException e) {
/* 594 */       this.m_serviceContainer = null;
/* 595 */       throw e;
/*     */     }
/*     */   }
/*     */
/*     */   public Properties getEnvironmentProperties() {
/*     */     try {
/* 601 */       return getRemoteServiceContainer().getEnvironmentProperties();
/*     */     } catch (GXRemoteException e) {
/* 603 */       this.m_serviceContainer = null;
/* 604 */       throw e;
/*     */     }
/*     */   }
/*     */
/*     */   public void setEnvironmentProperties(Properties prp) {
/*     */     try {
/* 610 */       getRemoteServiceContainer().setEnvironmentProperties(prp);
/*     */     } catch (GXRemoteException e) {
/* 612 */       this.m_serviceContainer = null;
/* 613 */       throw e;
/*     */     }
/*     */   }
/*     */
/*     */   public GXIPerformanceTraceManager getPerformanceTraceManager() {
/*     */     try {
/* 619 */       return getRemoteServiceContainer().getPerformanceTraceManager();
/*     */     } catch (GXRemoteException e) {
/* 621 */       this.m_serviceContainer = null;
/* 622 */       throw e;
/*     */     }
/*     */   }
/*     */
/*     */   public boolean isSecured() {
/* 627 */     return this.m_secured;
/*     */   }
/*     */
/*     */   public void setSecured(boolean secured) {
/* 631 */     this.m_secured = secured;
/*     */   }
/*     */
/*     */   public GXCustomCounters getCustomPerfCounters() {
/*     */     try {
/* 636 */       return getRemoteServiceContainer().getCustomPerfCounters();
/*     */     } catch (GXRemoteException e) {
/* 638 */       this.m_serviceContainer = null;
/* 639 */       throw e;
/*     */     }
/*     */   }
/*     */
/*     */   public void setCustomPerfCounters(GXCustomCounters counters) {
/*     */     try {
/* 645 */       getRemoteServiceContainer().setCustomPerfCounters(counters);
/*     */     } catch (GXRemoteException e) {
/* 647 */       this.m_serviceContainer = null;
/* 648 */       throw e;
/*     */     }
/*     */   }
/*     */
/*     */   public GXILogEventsReader<GXILogEvent.GXIPerfAlertsLogEvent> getPerfAlertsLogReader() {
/*     */     try {
/* 654 */       return getRemoteServiceContainer().getPerfAlertsLogReader();
/*     */     } catch (GXRemoteException e) {
/* 656 */       this.m_serviceContainer = null;
/* 657 */       throw e;
/*     */     }
/*     */   }
/*     */
/*     */   public boolean isWindows() {
/*     */     try {
/* 663 */       return getRemoteServiceContainer().isWindows();
/*     */     } catch (GXRemoteException e) {
/* 665 */       this.m_serviceContainer = null;
/* 666 */       throw e;
/*     */     }
/*     */   }
/*     */
/*     */   public GXIPreferences getPreferences(String folderName, String fileName) {
/*     */     try {
/* 672 */       return getRemoteServiceContainer().getPreferences(folderName, fileName);
/*     */     }
/*     */     catch (GXRemoteException localGXRemoteException) {}
/* 675 */     return null;
/*     */   }
/*     */
/*     */   public void savePreferences(GXIPreferences pref, String folderName, String fileName) {
/*     */     try {
/* 680 */       getRemoteServiceContainer().savePreferences(pref, folderName, fileName);
/*     */     }
/*     */     catch (GXRemoteException localGXRemoteException) {}
/*     */   }
/*     */
/*     */  

/*
public String getCustomConfiguration(String plugiName, String configName) throws Exception {
   return getRemoteServiceContainer().getCustomConfiguration(plugiName, configName);
  }
*/
}